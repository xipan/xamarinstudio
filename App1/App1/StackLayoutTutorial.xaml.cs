﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Windows.Input;


namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StackLayoutTutorial : ContentPage
    {

        public ICommand TapCommand { get; private set; }

        public StackLayoutTutorial()
        {
            InitializeComponent();
            TapCommand = new Command(async () => await DisplayAlert("Tapped", "This is a tapped Span.", "OK"));
        }

        private async void OnNewProfileButtonClick(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new NewProfile());
            //await DisplayAlert("Tapped", "This is a tapped Span.", "OK");

            await Navigation.PushModalAsync(new NewProfile());
        }

        private async void OnTestClick(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            User item;

            var uri = new Uri(string.Format("http://10.0.6.13/api/user/1"));
            var response = await client.GetAsync(uri);
            var content = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                AllowTrailingCommas = true
            };
            item = JsonSerializer.Deserialize<User>(content, options);

            //string text = await client.GetStringAsync("http://10.0.6.13/api/user/1");

            this.rest.Text = item.Email;
            await DisplayAlert("Tapped", "This is a tapped Span.", "OK");
        }

        public ICommand ClickCommand => new Command<string>((url) =>
        {
            Device.OpenUri(new System.Uri(url));
        });



    }

    public class User
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string AfterName { get; set; }
        public string Email { get; set; }
        public string PassWord { get; set; }
        /// <summary>
        /// UserType 0: employee, 1: customer
        /// </summary>
        public int userType { get; set; }
    }

}