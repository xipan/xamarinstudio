﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ToolbarItem : ContentPage
    {
        public ToolbarItem()
        {
            InitializeComponent();
        }

        private void OnIconButtonClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new StackLayoutTutorial());
        }
    }
}